
/*
  |>> 默认首页：[元素]模块 —— basics
 */

Page({
  data: {
    PageCur: 'basics'
  },
  NavChange(e) {
    console.log('|> ------- [NavChange]');
    console.log(e);

    this.setData({
      PageCur: e.currentTarget.dataset.cur
    })
  },
  onShareAppMessage() {
    return {
      title: 'ColorUI-高颜值的小程序UI组件库',
      imageUrl: '/images/share.jpg',
      path: '/pages/index/index'
    }
  },
})